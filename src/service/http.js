import axios from 'axios';

export default baseURL => axios.create({
  baseURL,
  timeout: 50000,
  headers: {
  },
});
