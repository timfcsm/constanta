/* eslint-disable import/prefer-default-export */
export function isObjectEqual(obj1, obj2) {
  return JSON.stringify(obj1) === JSON.stringify(obj2);
}

export function delay(timeout = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
}

export function poll(fn, retries = Infinity, timeoutBetweenAttempts = 1000) {
  const isRunning = true;
  // eslint-disable-next-line no-underscore-dangle
  let _retries = retries;

  return Promise.resolve()
    .then(async () => {
      if (isRunning) {
        try {
          await fn();
          await delay(timeoutBetweenAttempts);
          poll(fn, _retries, timeoutBetweenAttempts);
        } catch (e) {
          throw e;
        }
      }
    })
    .catch(async function retry(err) {
      // eslint-disable-next-line no-plusplus
      if (_retries-- > 0) {
        try {
          await delay(timeoutBetweenAttempts);
          await fn();
        } catch (e) {
          await retry(e);
        }
      }
      throw err;
    });
}
