import Vue from 'vue';
// eslint-disable-next-line no-unused-vars
import foundationStyles from 'foundation-sites/dist/css/foundation.css';
// eslint-disable-next-line no-unused-vars
import foundationPrototypeStyles from 'foundation-sites/dist/css/foundation-prototype.css';
// eslint-disable-next-line no-unused-vars
import materialIcons from 'material-icons/iconfont/material-icons.css';

import App from './App.vue';
import router from './router';
import store from './store/store';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
