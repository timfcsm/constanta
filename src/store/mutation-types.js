/* eslint-disable import/prefer-default-export */
// shop
export const SET_GOODS = 'SET_GOODS';
export const SET_NEXT_GOODS_PAGE = 'SET_NEXT_GOODS_PAGE';
export const SET_PREV_GOODS_PAGE = 'SET_PREV_GOODS_PAGE';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const SET_FILTER_PARAMS = 'SET_FILTER_PARAMS';
export const SET_CHECKOUT = 'SET_CHECKOUT';
export const EMPTY_CART = 'EMPTY_CART';
