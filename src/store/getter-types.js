/* eslint-disable import/prefer-default-export */
// shop
export const GET_CART_COUNT = 'GET_CART_COUNT';
export const GET_CART_TOTAL = 'GET_CART_TOTAL';
export const GET_ITEMS_IN_CART = 'GET_ITEMS_IN_CART';
export const GET_FILTER_DEFAULTS = 'GET_FILTER_DEFAULTS';
