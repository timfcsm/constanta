import * as mutations from './mutation-types';
import * as actions from './action-types';
import * as getters from './getter-types';

import http from '@/service/http';

export default {
  namespaced: true,
  state() {
    return {
      goods: [],
      goodsNext: '/starships/',
      goodsPrev: null,
      cart: [],
      checkout: false,
      filterData: {
        cost_in_credits: {
          type: 'range',
          values: null,
        },
        hyperdrive_rating: {
          type: 'range',
          values: null,
        },
        starship_class: {
          type: 'checkbox',
          values: [],
        },
      },
    };
  },
  mutations: {
    [mutations.SET_GOODS](state, goods) {
      state.goods = goods;
    },
    [mutations.SET_NEXT_GOODS_PAGE](state, url) {
      state.goodsNext = url;
    },
    [mutations.ADD_TO_CART](state, id) {
      if (state.cart.indexOf(id) === -1) {
        state.cart.push(id);
      }
    },
    [mutations.REMOVE_FROM_CART](state, id) {
      state.cart.splice(state.cart.indexOf(id), 1);
    },
    [mutations.SET_FILTER_PARAMS](state, { name, values }) {
      const data = state.filterData;
      if (data.type === 'range') {
        // eslint-disable-next-line no-param-reassign
        values[0] = Number(values[0]);
        // eslint-disable-next-line no-param-reassign
        values[1] = Number(values[1]);
      }
      if (data[name]) data[name].values = values;
    },
    [mutations.SET_CHECKOUT](state, val) {
      state.checkout = val;
    },
    [mutations.EMPTY_CART](state) {
      state.cart = [];
    },
  },
  actions: {
    [actions.CHECKOUT_CART]({ commit }) {
      commit(mutations.SET_CHECKOUT, false);
      commit(mutations.EMPTY_CART);
    },
    async [actions.FETCH_GOODS]({ commit, state, rootState }) {
      let result = [];

      commit(mutations.SET_NEXT_GOODS_PAGE, '/starships/');

      while (state.goodsNext) {
        try {
          // eslint-disable-next-line no-await-in-loop
          const { data } = await http(rootState.baseURL).get(state.goodsNext);

          if (data.results) {
            result = result.concat(data.results);
          }

          const next = typeof data.next === 'string'
            ? data.next.replace(rootState.baseURL, '')
            : null;

          commit(mutations.SET_NEXT_GOODS_PAGE, next);
        } catch (e) {
          commit(mutations.SET_NEXT_GOODS_PAGE, null);
        }
      }

      commit(mutations.SET_GOODS, result);
      commit(mutations.SET_NEXT_GOODS_PAGE, '/starships/');
    },
  },
  getters: {
    [getters.GET_CART_COUNT](state) {
      return state.cart.length;
    },
    [getters.GET_CART_TOTAL](state) {
      return state.cart.reduce((acc, id) => {
        const item = state.goods.find(good => good.url === id);
        if (item) {
          return acc + (Number(item.cost_in_credits) || 0);
        }

        return acc;
      }, 0);
    },
    [getters.GET_ITEMS_IN_CART](state) {
      const result = [];
      state.cart.forEach((id) => {
        const good = state.goods.find(_good => _good.url === id);

        if (good) {
          result.push(good);
        }
      });

      return result;
    },
    [getters.GET_FILTER_DEFAULTS](state) {
      const { goods, filterData } = state;
      const filterNames = Object.keys(filterData);
      const result = {};

      if (!goods.length) return null;

      filterNames.forEach((filterName) => {
        result[filterName] = {};
        let values = goods.map(item => item[filterName])
          .filter((val, i, self) => self.indexOf(val) === i);

        if (filterData[filterName].type === 'range') {
          values = values.filter(Number);
          result[filterName] = [Math.min(...values), Math.max(...values)];
        } else {
          result[filterName] = values;
        }
      });

      return result;
    },
  },
};
