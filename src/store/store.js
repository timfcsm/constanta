import Vue from 'vue';
import Vuex from 'vuex';

import shop from '@/store/shop';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    shop,
  },
  state: {
    baseURL: 'https://swapi.co/api',
  },
  mutations: {

  },
  actions: {

  },
});
